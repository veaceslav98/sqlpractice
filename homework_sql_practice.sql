-- Write a query to display: 
-- 1. the first name, last name, department number, and department name for each employee.
SELECT FIRST_NAME, LAST_NAME, DEPARTMENT_ID, DEPARTMENT_NAME
FROM EMPLOYEES E LEFT JOIN DEPARTMENTS D USING(DEPARTMENT_ID);

-- 2. the first and last name, department, city, and state province for each employee.    
SELECT e.first_name, e.last_name, department_id, d.department_name, l.city, l.state_province
FROM employees e
    LEFT JOIN departments d using(department_id)
    LEFT JOIN locations l using(location_id);

-- 3. the first name, last name, salary, and job grade for all employees.
SELECT emp.first_name, emp.last_name, emp.salary, j.job_title
FROM employees emp
    LEFT JOIN jobs j using(job_id);

-- 4. the first name, last name, department number and department name, for all employees for departments 80 or 40.
SELECT emp.first_name, emp.last_name, department_id, d.department_name
FROM employees emp 
LEFT JOIN departments d USING(department_id)
    WHERE department_id = 40 OR department_id = 80;

-- 5. those employees who contain a letter z to their first name and also display their last name, department, city, and state province.
SELECT emp.first_name, emp.last_name, department_id, d.department_name, l.city, l.state_province
FROM employees emp  
    LEFT JOIN departments d USING(department_id)
    LEFT JOIN locations l USING(location_id) 
        WHERE LOWER(emp.first_name) like '%z%';

-- 6. all departments including those where does not have any employee.
SELECT department_id, d.department_name, emp.first_name
FROM employees emp 
    RIGHT JOIN departments d USING(department_id);

-- 7. the first and last name and salary for those employees who earn less than the employee earn whose number is 182.
SELECT employee_id, emp.first_name, emp.last_name, emp.salary 
FROM employees emp 
    WHERE emp.salary > (SELECT salary FROM employees WHERE employee_id = 182);

-- 8. the first name of all employees including the first name of their manager.
SELECT emp.employee_id, emp.first_name AS first_name, emp.last_name, emp2.first_name AS manager_name, emp2.last_name AS manager_last_name    --- write the same with join
FROM employees emp, employees emp2
    WHERE emp2.employee_id = emp.manager_id;

-- 9. the department name, city, and state province for each department.
SELECT dep.department_name, loc.city, loc.state_province
FROM departments dep 
    LEFT JOIN locations loc USING(location_id);

--10. the first name, last name, department number and name, for all employees who have or have not any department.
SELECT emp.first_name, emp.last_name, department_id, dep.department_name 
FROM employees emp
    LEFT JOIN departments dep USING (department_id);

--11. the first name of all employees and the first name of their manager including those who does not working under any manager.    
SELECT emp.employee_id, emp.first_name AS employee_first_name, emp2.first_name AS manager_first_name, emp2.last_name AS manager_last_name
FROM employees emp, employees emp2
    WHERE emp2.employee_id = emp.manager_id 
    OR emp2.manager_id IS NULL;

--12. the first name, last name, and department number for those employees who works in the same department as the employee who holds the last name as Taylor.
SELECT first_name, last_name, department_id
FROM employees
    WHERE last_name = 'Taylor' ;

SELECT DISTINCT(emp.first_name), emp.last_name, emp.department_id 
FROM employees emp, employees emp2
    WHERE emp.department_id = emp2.department_id
    AND emp.last_name = 'Taylor';
    
--13. the job title, department name, full name (first and last name ) of employee, 
--and starting date for all the jobs which started on or after 1st January, 1993 and ending with on or before 31 August, 1997.
SELECT j.job_title, dep.department_name, emp.first_name, emp.last_name, start_date, end_date   ---?
FROM employees emp
    LEFT JOIN departments dep using(department_id)
    LEFT JOIN jobs j using(job_id)
    LEFT JOIN job_history using(employee_id)
    WHERE start_date >= '01-01-1993'
    AND end_date <= '31-08-1997';
    
    
SELECT j.job_title, dep.department_name, emp.first_name, emp.last_name, start_date, end_date   ---?
FROM employees emp
    LEFT JOIN departments dep using(department_id)
    LEFT JOIN jobs j using(job_id)
    LEFT JOIN job_history using(employee_id)
    WHERE hire_date between '01-01-1993' AND '31-08-1997';

--14. job title, full name (first and last name ) of employee, and the difference between maximum salary for the job and salary of the employee.
SELECT j.job_title, emp.first_name, emp.last_name, (j.max_salary - emp.salary) AS difference_salary
FROM employees emp 
    LEFT JOIN jobs j USING(job_id);

--15. the name of the department, average salary and number of employees working in that department who got commission.
SELECT dep.department_name, COUNT(employee_id) AS employees_number, AVG(salary) AS average_salary
FROM employees emp
    LEFT JOIN departments dep USING(department_id)
    GROUP BY dep.department_name;

--16. the full name (first and last name ) of employee, and job title of those employees who is working in the department which ID is 80.
SELECT emp.first_name, emp.last_name, j.job_title
FROM employees emp
LEFT JOIN jobs j USING(job_id)
    WHERE department_id = 80;

--17. the name of the country, city, and the departments which are running there.
SELECT dep.department_name, loc.city, c.country_name, department_id
FROM departments dep
    LEFT JOIN locations loc USING(location_id)
    LEFT JOIN countries c USING(country_id);

--18. department name and the full name (first and last name) of the manager.
SELECT dep.department_name, man.first_name AS manager_first_name, man.last_name AS manager_last_name
FROM employees emp, employees man
    LEFT JOIN departments dep USING(department_id)
    WHERE man.employee_id = emp.manager_id;

--19. job title and average salary of employees.
SELECT j.job_title, AVG(emp.salary) AS average_salary
FROM employees emp
    LEFT JOIN jobs j USING(job_id)
    GROUP BY j.job_title;

--20. the details of jobs which was done by any of the employees who is presently earning a salary on and above 12000.
SELECT emp.first_name, emp.last_name, emp.salary, job_id, j.job_title, department_id, dep.department_name, loc.city, loc.state_province, c.country_name
FROM employees emp
    LEFT JOIN jobs j USING(job_id)
    LEFT JOIN departments dep USING(department_id)
    LEFT JOIN locations loc USING(location_id)
    LEFT JOIN countries c using(country_id)
        WHERE emp.salary >= 12000;

--21. the country name, city, and number of those departments where at leaste 2 employees are working.
SELECT c.country_name, loc.city, CASE WHEN COUNT(employee_id) >= 2 THEN COUNT(department_id) END AS departments_number 
FROM employees emp
    LEFT JOIN departments dep USING(department_id)
    LEFT JOIN locations loc USING(location_id)
    LEFT JOIN countries c USING(country_id)
    GROUP BY c.country_name, loc.city;

--22. the department name, full name (first and last name) of manager, and their city.
SELECT dep.department_name, man.first_name AS manager_first_name, man.last_name AS manager_last_name, loc.city
FROM employees emp, employees man
    LEFT JOIN departments dep USING(department_id)
    LEFT JOIN locations loc USING(location_id)
    WHERE man.employee_id = emp.manager_id;
    
--23. the employee ID, job name, number of days worked in for all those jobs in department 80.
SELECT employee_id, job_title AS job_name, department_name, department_id        ------ I dont understand second part of task 
FROM employees emp
    LEFT JOIN jobs j using(job_id)
    LEFT JOIN departments dep USING(department_id)
    WHERE department_id = 80;

--24. the full name (first and last name), and salary of those employees who working in any department located in London.
SELECT emp.first_name, emp.last_name, emp.salary, loc.city
FROM employees emp
    LEFT JOIN departments dep USING(department_id)
    LEFT JOIN locations loc USING(location_id)
    WHERE loc.city in 'London';

--25. full name(first and last name), job title, starting and ending date of last jobs for those employees with worked without a commission percentage.
SELECT emp.first_name, emp.last_name, j.job_title, jh.start_date, jh.end_date, commission_pct
FROM employees emp
    LEFT JOIN jobs j using(job_id)
    LEFT JOIN job_history jh using(employee_id)
    WHERE commission_pct IS NULL;
    
--26. the department name and number of employees in each of the department.
SELECT dep.department_name, COUNT(emp.employee_id)
FROM employees emp
    LEFT JOIN departments dep USING(department_id) 
    GROUP BY dep.department_name;

--27. the full name (firt and last name ) of employee with ID and name of the country presently where (s)he is working.
SELECT emp.first_name, emp.last_name, employee_id, c.country_name, country_id
FROM employees emp
    LEFT JOIN departments dep using(department_id)
    LEFT JOIN locations loc using(location_id)
    LEFT JOIN countries c using(country_id);

--28. the name ( first name and last name ) for those employees who gets more salary than the employee whose ID is 163.
SELECT emp.first_name, emp.last_name, emp.salary
FROM employees emp 
    WHERE emp.salary > (SELECT salary FROM employees WHERE employee_id = 163);

--29. the name ( first name and last name ), salary, department id, job id for those employees who works in the same designation as the employee works whose id is 169.
SELECT emp.first_name, emp.last_name, emp.salary, department_id, job_id                                              ---- hope I understand correct
FROM employees emp 
    LEFT JOIN departments dep USING(department_id)
    LEFT JOIN jobs j USING(job_id)
    WHERE department_id = (SELECT department_id from employees WHERE employee_id = 169);

--30. the name ( first name and last name ), salary, department id for those employees who earn such amount of salary which is the smallest salary of any of the departments.
SELECT first_name, last_name, salary, department_id
FROM employees emp
    LEFT JOIN departments dep USING(department_id)
    WHERE salary in (SELECT MIN(salary)FROM employees);

--31. the employee id, employee name (first name and last name ) for all employees who earn more than the average salary. 
SELECT employee_id, emp.first_name, emp.last_name, emp.salary
FROM employees emp
    WHERE emp.salary > (SELECT AVG(salary) FROM employees);

--32. the employee name ( first name and last name ), employee id and salary of all employees who report to Payam. 
SELECT emp.first_name, emp.last_name, employee_id, emp.salary
FROM employees emp
    WHERE emp.manager_id = (SELECT employee_id FROM employees WHERE first_name = 'Payam');

--33. the department number, name ( first name and last name ), job and department name for all employees in the Finance department.
SELECT department_id, emp.first_name, emp.last_name, j.job_title, dep.department_name
FROM employees emp
    LEFT JOIN departments dep USING(department_id)
    LEFT JOIN jobs j USING(job_id)
    WHERE department_name = 'Finance';

--34. all the information of an employee whose salary and reporting person id is 3000 and 121 respectively. 
SELECT employee_id, emp.first_name, emp.last_name , salary, manager_id              ---- Hope I understand correct
FROM employees emp
    WHERE emp.salary = 3000
    AND emp.manager_id = (SELECT employee_id FROM employees WHERE employee_Id = 121);

--35. all the information of an employee whose id is any of the number 134, 159 and 183. Go to the editor 
SELECT employee_id, first_name, last_name, salary
FROM employees emp
    WHERE employee_id = 134
    OR employee_id = 159
    OR employee_id = 183;

--36. all the information of the employees whose salary is within the range 1000 and 3000. 
SELECT employee_id, first_name, last_name, salary
FROM employees emp
    WHERE salary BETWEEN 1000 AND 3000;

--37. all the information of the employees whose salary is within the range of smallest salary and 2500. 
SELECT employee_id, first_name, last_name, salary 
FROM employees emp
    WHERE emp.salary BETWEEN (SELECT MIN(salary) FROM employees) AND 2500;
    
SELECT employee_id, first_name, last_name, salary 
FROM employees emp
    WHERE emp.salary > (SELECT MIN(salary) FROM employees) 
    AND emp.salary < 2500;

--38. all the information of the employees who does not work in those departments where some employees works whose id within the range 100 and 200. 
SELECT employee_id, first_name, last_name, salary 
FROM employees emp
    WHERE emp.department_id not in (SELECT department_id from departments WHERE employee_id BETWEEN 100 and 200);
    
--39. all the information for those employees whose id is any id who earn the second highest salary. 
SELECT employee_id, first_name, last_name, salary
FROM employees emp
    WHERE emp.salary < (SELECT MAX(salary) FROM employees WHERE salary NOT IN (SELECT MAX(salary) FROM employees));
    
--40. the employee name( first name and last name ) and hiredate for all employees in the same department as Clara. Exclude Clara. 
SELECT emp.first_name, emp.last_name, emp.hire_date , department_id
FROM employees emp
    WHERE department_id = (SELECT department_id FROM employees WHERE first_name = 'Clara')
    AND emp.first_name != 'Clara';
    
--41. the employee number and name( first name and last name ) for all employees who work in a department with any employee whose name contains a T. 
SELECT employee_id, emp.first_name, emp.last_name, emp.salary, department_id
FROM employees emp
    WHERE department_id IN (SELECT department_id FROM departments WHERE LOWER(emp.first_name) like '%t%');

--42. the employee number, name( first name and last name ), and salary for all employees who earn more than the average salary and 
--who work in a department with any employee with a J in their name. 
SELECT employee_id, emp.first_name, emp.last_name, emp.salary
FROM employees emp
    WHERE department_id IN (SELECT department_id FROM departments WHERE LOWER(emp.first_name) like '%j%')
    AND emp.salary > (SELECT AVG(salary) FROM employees);

--43. the employee name( first name and last name ), employee id, and job title for all employees whose department location is Toronto.
SELECT emp.first_name, emp.last_name, employee_id, j.job_title, loc.city
FROM employees emp
    LEFT JOIN departments dep using(department_id)
    LEFT JOIN jobs j using(job_id)
    LEFT JOIN locations loc using(location_id)
    WHERE loc.city = 'Toronto';

--44. the employee number, name( first name and last name ) and job title for all employees whose salary is smaller than any salary of those employees whose job title is MK_MAN.
SELECT employee_id, first_name, last_name, job_title, job_id
FROM employees emp
    LEFT JOIN jobs j using(job_id)
    WHERE emp.salary <= (SELECT salary FROM employees WHERE job_id = 'MK_MAN');

--45. the employee number, name( first name and last name ) and job title for all employees whose salary is smaller than any salary of 
--those employees whose job title is MK_MAN. Exclude Job title MK_MAN.  
SELECT employee_id, first_name, last_name, job_title, job_id
FROM employees emp
    LEFT JOIN jobs j USING(job_id)
    WHERE emp.salary < (SELECT salary FROM employees WHERE job_id = 'MK_MAN');

--46. all the information of those employees who did not have any job in the past. 
--- Dont ungerstand task

--47. the employee number, name( first name and last name ) and job title for all employees whose salary is more than any average salary of any department. 
----- ???????

--48. the employee name( first name and last name ) and department for all employees for any existence of those employees whose salary is more than 3700.  
----Dont understand task

--49. the department id and the total salary for those departments which contains at least one salaried employee.  
SELECT * FROM employees emp RIGHT JOIN departments dep USING(department_id);
SELECT department_id, SUM(salary) AS total_salary
FROM employees emp
    LEFT JOIN departments dep USING(department_id) 
    GROUP BY department_id;

--50. the employee id, name ( first name and last name ) and the job id column with a modified title SALESMAN for those employees 
--whose job title is ST_MAN and DEVELOPER for whose job title is IT_PROG.  
SELECT employee_id, first_name, last_name,
CASE 
    WHEN job_id = 'ST_MAN' THEN 'SALESMAN'
    WHEN job_id = 'IT_PROG' THEN 'DEVELOPER'
    ELSE job_id
    END AS job_id
FROM employees emp
    LEFT JOIN jobs USING(job_id)
    ORDER BY job_id;   
    
--51. the employee id, name ( first name and last name ), salary and the SalaryStatus column with a title HIGH and LOW 
--respectively for those employees whose salary is more than and less than the average salary of all employees.
SELECT employee_id, first_name, last_name, salary,
CASE
    WHEN salary > (SELECT AVG(salary) FROM employees) THEN 'HIGH'
    WHEN salary < (SELECT AVG(salary) FROM employees) THEN 'LOW'
    END AS salaryStatus
FROM employees emp 
GROUP BY employee_id, first_name, last_name, salary;

--52. the employee id, name ( first name and last name ), SalaryDrawn, AvgCompare (salary - the average salary of all employees) 
--and the SalaryStatus column with a title HIGH and LOW respectively for those employees whose salary is more than and less 
--than the average salary of all employees.  
                                                            ------ Dont understant first part of task 

--53. a set of rows to find all departments that do actually have one or more employees assigned to them. 
SELECT *                                            -------????
FROM employees emp
    LEFT JOIN departments dep USING(department_id)
    WHERE department_id IS NOT NULL;

--54. all employees who work in departments located in the United Kingdom.  
SELECT employee_id, emp.first_name, emp.last_name, department_id, dep.department_name, c.country_name 
FROM employees emp
    LEFT JOIN departments dep USING(department_id)
    LEFT JOIN locations loc USING(location_id)
    LEFT JOIN countries c USING(country_id)
    WHERE country_name = 'United Kingdom';

--55. all the employees who earn more than the average and who work in any of the IT departments.   
SELECT employee_id, first_name, last_name, department_name
FROM employees emp
    LEFT JOIN departments dep USING(department_id)
    WHERE emp.salary > (SELECT AVG(salary) FROM employees)
    AND LOWER(department_name) like '%it%';

--56. who earns more than Mr. Ozer.
SELECT employee_id, emp.first_name, emp.last_name
FROM employees emp
    WHERE emp.salary > (SELECT salary FROM employees WHERE last_name = 'Ozer');

--57. which employees have a manager who works for a department based in the US.
SELECT emp.first_name, emp.last_name, country_id
FROM employees emp, employees man
    LEFT JOIN departments dep using(department_id)
    LEFT JOIN locations loc using(location_id)
    LEFT JOIN countries c using(country_id)
    WHERE  man.employee_id = emp.manager_id
    AND country_id = 'US';

--58. the names of all employees whose salary is greater than 50% of their departments total salary bill.
SELECT employee_id, emp.first_name, emp.last_name, emp.salary, dep.department_name
FROM employees emp
    LEFT JOIN departments dep using(department_id)
    WHERE emp.salary > (SELECT SUM(salary) FROM employees )/50;
    GROUP BY dep.department_name;
    
SELECT SUM(salary)/2, department_name
FROM employees emp
    LEFT JOIN departments dep using(department_id) group by department_name;

--59. the details of employees who are managers. 
SELECT dep.department_name, man.first_name AS manager_first_name, man.last_name AS manager_last_name
FROM employees emp, employees man
    LEFT JOIN departments dep USING(department_id)
    WHERE man.employee_id = emp.manager_id;

--60. the details of employees who manage a department.
SELECT employee_id, emp.first_name, emp.last_name, department_id, dep.department_name
FROM employees emp
    LEFT JOIN departments dep using(department_id)
    WHERE employee_id = dep.manager_id;

--61. the employee id, name ( first name and last name ), salary, department name and 
--city for all the employees who gets the salary as the salary earn by the employee which 
--is maximum within the joining person January 1st, 2002 and December 31st, 2003. 
SELECT employee_id, emp.first_name, emp.last_name, emp.salary, dep.department_name, loc.city
FROM employees emp
    LEFT JOIN departments dep using(department_id)
    LEFT JOIN locations loc using(location_id) 
    WHERE salary = (SELECT MAX(salary) FROM employees WHERE hire_date BETWEEN '01-01-2002' AND '31-12-2003');
    
--62. the department code and name for all departments which located in the city London.
SELECT department_id, department_name, city
FROM departments dep
    LEFT JOIN locations loc using(location_id)
    WHERE city = 'London';

--63. the first and last name, salary, and department ID for all those employees who 
--earn more than the average salary and arrange the list in descending order on salary. 
SELECT first_name, last_name, salary, department_id
FROM employees emp
    LEFT JOIN departments dep using(department_id)
    WHERE emp.salary > (SELECT AVG(salary) FROM employees)
    order by salary desc;
    
--64. the first and last name, salary, and department ID for those employees who 
--earn more than the maximum salary of a department which ID is 40. 
SELECT first_name, last_name, salary, department_id
FROM employees emp
    LEFT JOIN departments dep using(department_id)
    WHERE emp.salary > (SELECT max(salary) FROM employees where department_id = '40')
    order by salary desc;

--65. the department name and Id for all departments where they located, that Id 
--is equal to the Id for the location where department number 30 is located. 
SELECT dep.department_name, department_id, loc.city
FROM departments dep 
    LEFT JOIN locations loc USING(location_id)
    WHERE loc.city IN (SELECT city FROM locations WHERE department_id = 30);
    
--66. the first and last name, salary, and department ID for all those employees
--who work in that department where the employee works who hold the ID 201. 
SELECT emp.first_name, emp.last_name, emp.salary, department_id, employee_id
FROM employees emp
    LEFT JOIN departments dep using(department_id)
    WHERE department_id IN (SELECT department_id FROM employees WHERE employee_id = 201);

--67. the first and last name, salary, and department ID for those employees whose 
--salary is equal to the salary of the employee who works in that department which ID is 40. 
SELECT emp.first_name, emp.last_name, salary, department_id, employee_id
FROM employees emp
    LEFT JOIN departments dep using(department_id)
    WHERE salary in (SELECT salary FROM employees WHERE department_id = 40);

--68. the first and last name, and department code for all employees who work in the department Marketing. 
SELECT emp.first_name, emp.last_name, department_id, dep.department_name
FROM employees emp
    LEFT JOIN departments dep using(department_id)
    WHERE department_name in 'Marketing';

--69. the first and last name, salary, and department ID for those employees who 
--earn more than the minimum salary of a department which ID is 40. 
SELECT first_name, last_name, salary, department_id
FROM employees emp 
    LEFT JOIN departments dep using(department_id)
    WHERE salary > (SELECT MIN(salary) FROM employees WHERE department_id = 40);
    
--70. the full name,email, and designation for all those employees who was hired after the employee whose ID is 165. 
SELECT first_name, last_name, email
FROM employees emp
    WHERE hire_date > (SELECT hire_date FROM employees WHERE employee_id = 165);

--71. the first and last name, salary, and department ID for those employees who earn less than the minimum salary of a department which ID is 70. 
SELECT emp.first_name, emp.last_name, emp.salary, department_id
FROM employees emp 
    LEFT JOIN departments dep using(department_id)
    WHERE salary < (SELECT MIN(salary) FROM employees WHERE department_id = 70);

--72. the first and last name, salary, and department ID for those employees who 
--earn less than the average salary, and also work at the department where the employee 
--Laura is working as a first name holder. 
SELECT first_name, last_name, salary, department_id
FROM employees emp
    WHERE salary < (SELECT AVG(salary) FROM employees)
    AND department_id = (SELECT department_id FROM employees WHERE first_name = 'Laura');

--73. the city of the employee whose ID 134 and works there.
SELECT city 
FROM employees emp
    LEFT JOIN departments dep using(department_id)
    LEFT JOIN locations loc using(location_id)
    WHERE employee_id = 134;

--74. the the details of those departments which max salary is 7000 or above for those employees who already done one or more jobs.
SELECT department_id, department_name, city, state_province; ----- ?????

--75. the detail information of those departments which starting salary is at least 8000. 
SELECT department_id, department_name
FROM employees emp
    LEFT JOIN departments dep using(department_id)
    LEFT JOIN jobs j using(job_id)
    WHERE 8000 <= (SELECT MIN(min_salary) FROM jobs);

--76. the full name (first and last name) of manager who is supervising 4 or more employees.

--77. the details of the current job for those employees who worked as a Sales Representative in the past.    

--78. all the infromation about those employees who earn second lowest salary of all the employees. 

--79. the details of departments managed by Susan.  

--80. the department ID, full name (first and last name), salary for those employees who is highest salary drawar in a department. 
